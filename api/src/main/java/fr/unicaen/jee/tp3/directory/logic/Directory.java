package fr.unicaen.jee.tp3.directory.logic;

import java.util.*;


/**
 * Contrat abstrait d'un Directory (annuaire)
 */
public interface Directory {
		
	/** 
	 * Permet de récupérer un ensemble de personnes dans un ordre arbitraire.
	 * Le sous-ensemble des personnes à récupérer et donné par les paramètres offset et limit.
	 * L'ordre est arbitraire mais il est garanti qu'il sera le même pour tous les appels.
	 * @param offset Index de la première personne à récupérer 
	 * @param limit Nombre maximum de personnes à récupérer
	 */
	public Collection<Person> getPersons(long offset, long limit) throws DirectoryException;
	
	/**
	 * Retourne une personne donnée par son identifiant (id)
	 * @return La personne trouvée, ou null si l'identifiant n'existe pas
	 */
	public Person getPerson(long id) throws DirectoryException;
	
	/**
	 * Libère les ressources occupées par cette instance d'annuaire
	 */
	public void dispose() throws DirectoryException;
	
}
