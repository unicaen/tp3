package fr.unicaen.jee.tp3.directory.service;

import java.util.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import fr.unicaen.jee.tp3.directory.logic.*;


/**
 * @author Frédérik Bilhaut
 */
@RestController
public class DirectoryController {
		
	private final Directory backend; // Dépendance injectée
		
	public DirectoryController(Directory directory) throws DirectoryException {
		this.backend = directory; 
	}
	
	@RequestMapping("/persons")
    public Collection<RestObject<Person>> getPersons(long offset, long limit) throws DirectoryException {
		Collection<RestObject<Person>> result = new LinkedList<>();
		for(Person person : this.backend.getPersons(offset, limit))	
			result.add(new RestObject<>(person, linkTo(methodOn(DirectoryController.class).getPerson(person.getId())).toUri()));
        return result;
    }
	
	
	@RequestMapping("/person/{id}")
    public RestObject<Person> getPerson(@PathVariable long id) throws DirectoryException {
		Person person = this.backend.getPerson(id);
        return new RestObject<>(person, linkTo(methodOn(DirectoryController.class).getPerson(person.getId())).toUri());
	}
	
	
	
}
