package fr.unicaen.jee.tp3.directory.service;

import org.springframework.boot.builder.*;
import org.springframework.boot.web.support.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;

/**
 * @author Frédérik Bilhaut
 */
@SpringBootApplication
@ImportResource("classpath:spring-config.xml")
public class DirectoryApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DirectoryApplication.class);
    }

}

