# TP3 #

L'objectif de ce TP est de créer un service REST appuyé sur une base de donnée relationnelle, et manipulant des objets composites.

La communication avec la base de données se fera via JDBC. La base de donnée utilisée sera Apache Derby, un petite base de données simple implémentée totalement en Java. Elle est pratique à utiliser pour faire des tests, même si elle ne conviendra généralement pas en production. Mais de toute façon, grâce à JDBC, tout votre code sera totalement indépendant de la base de donnée utilisée.

Le service sera implémenté via Spring, et la sérialisation / dé-sérialisation se fera en JSON avec Jackson, ce qui est la configuration par défaut de Spring.

Voir l'énoncé complet sur le [Moodle](http://foad2.unicaen.fr/moodle/course/view.php?id=24560).