package fr.unicaen.jee.tp3.directory.service;

import java.util.*;
import org.springframework.web.bind.annotation.*;
import fr.unicaen.jee.tp3.directory.logic.*;

/**
 * @author Frédérik Bilhaut
 */
@RestController
public class DirectoryController {
	
	private final Directory backend; // dépendance injectée
	
	public DirectoryController(Directory directory) throws DirectoryException {
		this.backend = directory; 
	}
	
	@RequestMapping("/persons")
    public Collection<Person> getPersons(long offset, long limit) throws DirectoryException {
        return this.backend.getPersons(offset, limit);
    }
	
	
	@RequestMapping("/person")
    public Person getPerson(long id) throws DirectoryException {
        return this.backend.getPerson(id);
    }
	
	
	
}
