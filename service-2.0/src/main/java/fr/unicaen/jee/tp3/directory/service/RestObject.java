package fr.unicaen.jee.tp3.directory.service;

import java.net.*;
import fr.unicaen.jee.tp3.directory.logic.*;


/**
 * Classe représentant un DirectoryObject quelconque, accompagné de méta-données
 * specifiquement liées au protocole REST.
 * @author Frédérik Bilhaut
 */
public class RestObject<T extends DirectoryObject> {
	
	private final T data;
	private final URI uri;
	
	public RestObject(T data, URI uri) {
		this.data = data;
		this.uri = uri;
	}

	public T getData() {
		return data;
	}

	public URI getUri() {
		return uri;
	}
	
}
