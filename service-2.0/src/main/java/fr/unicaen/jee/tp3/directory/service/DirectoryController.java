package fr.unicaen.jee.tp3.directory.service;

import java.net.*;
import java.util.*;
import javax.servlet.http.*;
import org.springframework.web.bind.annotation.*;
import fr.unicaen.jee.tp3.directory.logic.*;


/**
 * @author Frédérik Bilhaut
 */
@RestController
public class DirectoryController {
		
	private final Directory backend; // Dépendance injectée
		
	public DirectoryController(Directory directory) throws DirectoryException {
		this.backend = directory; 
	}
	
	@RequestMapping("/persons")
    public Collection<RestObject<Person>> getPersons(long offset, long limit, HttpServletRequest request) throws DirectoryException {
		Collection<RestObject<Person>> result = new LinkedList<>();
		for(Person person : this.backend.getPersons(offset, limit))	
			result.add(new RestObject<>(person, makeObjectURI(request, "person", person.getId())));		
        return result;
    }
	
	
	@RequestMapping("/person/{id}")
    public RestObject<Person> getPerson(@PathVariable long id, HttpServletRequest request) throws DirectoryException {
		Person person = this.backend.getPerson(id);
        return new RestObject<>(person, makeObjectURI(request, "person", person.getId()));
    }
	
	/**
	 * Construit l'URI d'un objet donné par son type et son id
	 * Utilie la requête HTTP pour connaître le host / port / etc. du service
	 * @throws DirectoryException En cas d'erreur de syntawe dans la construction de l'URI
	 */
	private URI makeObjectURI(HttpServletRequest request, String type, long id) throws DirectoryException {		
		try {
			String objectPath = request.getContextPath() + "/" + type + "/" + id;
			return new URL("http", request.getServerName(), request.getLocalPort(), objectPath).toURI();
		}
		catch(MalformedURLException | URISyntaxException e) {
			throw new DirectoryException(e);
		}
	}
	
	
}
