package fr.unicaen.jee.tp3.directory.logic.db;

/**
 * @author Frédérik Bilhaut
 */
public interface Schema {
	
	static final String TABLE_PERSON = "Person";	
	static final String COL_PERSON_ID = "PersonId";
	static final String COL_PERSON_LAST_NAME = "LastName";
	static final String COL_PERSON_FIRST_NAME = "FirstName";
	
}
