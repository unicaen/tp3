package fr.unicaen.jee.tp3.directory.logic.db;

import java.sql.*;
import java.util.*;
import fr.unicaen.jee.tp3.directory.logic.*;

/**
 * Implémentation à base de connection JDBC.
 * Dans cette version, une seule connexion est partagée (on fera un pool plus tard, cf. PooledDatabaseDirectory)
 * Pour cette raison les méthodes utilisant la connexion doivent être synchronisées pour être ré-entrantes.
 * @author Frédérik Bilhaut
 */
public class DatabaseDirectory implements Directory, AutoCloseable {
		
	//!\ Cf. remarque ci-desssus
	private Connection connection;
	
	
	public DatabaseDirectory(String databaseURL) throws DirectoryException {
		this("org.apache.derby.jdbc.ClientDriver", databaseURL);
	}

	
	/**
	 * Construit une instance étant donnés le nom de la classe driver et l'URL JDBC	 
	 */
	public DatabaseDirectory(String driverClass, String databaseURL) throws DirectoryException {		
		try {
			Class.forName(driverClass);            
			this.connection = DriverManager.getConnection(databaseURL); 
		}
		catch(SQLException|ClassNotFoundException e) {
			throw new DirectoryException(e);
		}
	}
	
	
	@Override
	public synchronized Collection<Person> getPersons(long offset, long limit) throws DirectoryException {		
		String sql = String.format("SELECT * FROM %s ORDER BY %s OFFSET ? ROWS FETCH NEXT ? ROWS ONLY", Schema.TABLE_PERSON, Schema.COL_PERSON_ID);
		try(PreparedStatement stmt = this.connection.prepareStatement(sql)) {			
			stmt.setLong(1, offset);
			stmt.setLong(2, limit);
			
			try(ResultSet rs = stmt.executeQuery()) {														
				Collection<Person> result = new LinkedList<>();
				while(rs.next()) {
					Person person = new Person();
					person.setId(rs.getLong(Schema.COL_PERSON_ID));
					person.setFirstName(rs.getString(Schema.COL_PERSON_FIRST_NAME));
					person.setLastName(rs.getString(Schema.COL_PERSON_LAST_NAME));
					result.add(person);
				}			
				return result;
			}
		}
		catch(SQLException e) {
			throw new DirectoryException(e);
		}
	}	
	
	
	@Override
	public synchronized Person getPerson(long id) throws DirectoryException {
		String sql = String.format("SELECT * FROM %s WHERE %s=?", Schema.TABLE_PERSON, Schema.COL_PERSON_ID);
		try(PreparedStatement stmt = this.connection.prepareStatement(sql)) {			
			stmt.setLong(1, id);
			
			try(ResultSet rs = stmt.executeQuery()) {														
				
				if(!rs.next())
					return null;
								
				Person person = new Person();
				person.setId(id);
				person.setFirstName(rs.getString(Schema.COL_PERSON_FIRST_NAME));
				person.setLastName(rs.getString(Schema.COL_PERSON_LAST_NAME));
				return person;
			}
		}
		catch(SQLException e) {
			throw new DirectoryException(e);
		}
	}
	
	
	@Override
	public synchronized void dispose() throws DirectoryException {
		try {
			if(this.connection != null)
				this.connection.close();
			this.connection = null;
		}
		catch(SQLException e) {
			throw new DirectoryException(e);
		}
	}
	
	
	@Override
	public synchronized void close() throws Exception {
		dispose();
	}
	
}
