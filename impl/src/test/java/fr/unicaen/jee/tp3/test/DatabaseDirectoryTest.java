package fr.unicaen.jee.tp3.test;

import org.junit.*;
import fr.unicaen.jee.tp3.directory.logic.db.*;

/**
 * @author Frédérik Bilhaut
 */
public class DatabaseDirectoryTest {
	
	@Test
	public void loadPersons() throws Exception {
		try(DatabaseDirectory directory = new DatabaseDirectory("jdbc:derby://localhost:1527/Toto")) {
			directory.getPersons(1, 2).stream().forEach(x -> System.out.println(x));
		}
	}
	
}
